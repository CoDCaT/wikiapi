package com.codcat.wikiapi.articleListScreen

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.codcat.wikiapi.data.Article
import com.codcat.wikiapi.data.repository.IWikiRepository
import com.codcat.wikiapi.device.location.ILocationRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class ArticleViewModel @Inject constructor() : ViewModel(), CoroutineScope {

    @Inject lateinit var wikiRepository: IWikiRepository
    @Inject lateinit var locationRepository: ILocationRepository

    private lateinit var articles: MutableLiveData<MutableList<Article>>
    private lateinit var isLoading: MutableLiveData<Boolean>
    private lateinit var showError: MutableLiveData<Boolean>

    private val viewModelJob = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + viewModelJob

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun getArticles(): LiveData<MutableList<Article>> {
        if (::articles.isInitialized.not()) {
            articles = MutableLiveData()
            loadArticles()
        }
        return articles
    }

    fun getIsLoading(): LiveData<Boolean> {
        if (::isLoading.isInitialized.not()) {
            isLoading = MutableLiveData()
        }
        return isLoading
    }

    fun getShowError(): LiveData<Boolean> {
        if (::showError.isInitialized.not()) {
            showError = MutableLiveData()
        }
        return showError
    }

    private fun loadArticles() {
        launch {
            try {
                isLoading.value = true
                val location = locationRepository.getCurrentPosition()
                val result = wikiRepository.getWikiArticleList(location.latitude, location.longitude)
                result.await()?.let {
                    showError.value = false
                    articles.value = it
                }
            } catch (e: Exception) {
                showError.value = true
            } finally {
                isLoading.value = false
            }
        }
    }
}