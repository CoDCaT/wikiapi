package com.codcat.wikiapi.articleListScreen

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import com.codcat.wikiapi.adapter.IWikiClickListener
import com.codcat.wikiapi.adapter.WikiListAdapter
import com.codcat.wikiapi.common.ViewModelFactory
import com.codcat.wikiapi.common.ViewModelKey
import com.codcat.wikiapi.di.scopes.ActivityScope
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
abstract class MainActivityModule {

//    @Binds
//    @ActivityScope
//    abstract fun provideMainScreenActivity(activity: MainScreenActivity): MainScreenActivity

    @Binds
    @IntoMap
    @ViewModelKey(ArticleViewModel::class)
    internal abstract fun demoViewModel(viewModel: ArticleViewModel): ViewModel

    @Binds
    @ActivityScope
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @ActivityScope
    abstract fun bindIWikiClickListener(activity: MainScreenActivity): IWikiClickListener

    @Module
    companion object {

        @Provides
        @ActivityScope
        @JvmStatic
        fun provideWikiListAdapter(listener: IWikiClickListener): WikiListAdapter = WikiListAdapter(listener)

        @Provides
        @ActivityScope
        @JvmStatic
        fun providerLinearLayoutManager(context: MainScreenActivity): LinearLayoutManager = LinearLayoutManager(context)
    }

}