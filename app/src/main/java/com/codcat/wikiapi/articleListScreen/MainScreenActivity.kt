package com.codcat.wikiapi.articleListScreen

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.codcat.wikiapi.R
import com.codcat.wikiapi.adapter.IWikiClickListener
import com.codcat.wikiapi.adapter.WikiListAdapter
import com.codcat.wikiapi.data.Article
import com.codcat.wikiapi.detailScreen.DetailArticleActivity
import com.codcat.wikiapi.utilits.DialogManagerUtil
import com.codcat.wikiapi.utilits.injectViewModel
import com.codcat.wikiapi.utilits.permissionManager.QuickPermissionsOptions
import com.codcat.wikiapi.utilits.permissionManager.runWithPermissions
import com.codcat.wikiapi.utilits.visibleOrGone
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.progressBar
import kotlinx.android.synthetic.main.activity_main.recyclerWikiList
import kotlinx.android.synthetic.main.activity_main.toolbar
import javax.inject.Inject

class MainScreenActivity : DaggerAppCompatActivity(), IWikiClickListener {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject lateinit var linearLayoutManager: LinearLayoutManager
    @Inject lateinit var wikiListAdapter: WikiListAdapter

    private lateinit var viewModel: ArticleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = injectViewModel(viewModelFactory)
//        viewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)

        initViews()
        runWithPermissions(ACCESS_FINE_LOCATION,
            options = QuickPermissionsOptions(
                permissionsDeniedMethod = { showPermissionDialog() },
                permanentDeniedMethod = { showPermissionDialog() }
            ),
            callback = {
                observeViews()
            })
    }

    private fun showPermissionDialog() {
        DialogManagerUtil.showPermissionDialog(
            context = this,
            onDenied = { finish() },
            onConfirm = { navigateToPermissionSettings() })
    }

    private fun navigateToPermissionSettings() {
        Toast.makeText(this, "navigateToPermissionSettings", Toast.LENGTH_SHORT).show()
    }

    private fun initViews() {

        setSupportActionBar(toolbar)

        recyclerWikiList.apply {
            adapter = wikiListAdapter
            layoutManager = linearLayoutManager
        }
    }

    private fun observeViews() {

        viewModel.getArticles().observe(this, Observer { articles ->
            articles?.let {
                wikiListAdapter.setFileList(it)
            }
        })

        viewModel.getIsLoading().observe(this, Observer { isLoading ->
            isLoading?.let {
                progressBar.visibleOrGone(it)
            }
        })

        viewModel.getShowError().observe(this, Observer { show ->
            show?.let {
                if (show) Toast.makeText(this, getString(R.string.error_loading), Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onWikiClick(article: Article) {
        Intent(this, DetailArticleActivity::class.java).apply {
            startActivity(this)
        }
    }

    override fun onWIkiLongClick(article: Article) {

    }
}
