package com.codcat.wikiapi.di

import android.app.Application
import android.content.Context
import com.codcat.wikiapi.data.RepositoryModule
import com.codcat.wikiapi.device.DeviceModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [
        RepositoryModule::class,
        DeviceModule::class]
)
class ApplicationModule(private val appContext: Context) {

    @Provides
    @Singleton
    fun provideApplication(): Application = appContext as Application

    @Provides
    @Singleton
    fun providerContext(): Context = appContext

//    @Provides
//    @Singleton
//    fun providerPreferences(): IPreference = AppPreferences(appContext)
}