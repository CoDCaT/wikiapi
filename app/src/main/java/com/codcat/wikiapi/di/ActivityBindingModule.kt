package com.codcat.wikiapi

import com.codcat.wikiapi.articleListScreen.MainScreenActivity
import com.codcat.wikiapi.articleListScreen.MainActivityModule
import com.codcat.wikiapi.detailScreen.DetailArticleActivity
import com.codcat.wikiapi.detailScreen.DetailScreenModule
import com.codcat.wikiapi.di.scopes.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    fun bindMainActivity(): MainScreenActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [DetailScreenModule::class])
    fun bindDetailArticleActivity(): DetailArticleActivity
}