package com.codcat.wikiapi

import com.codcat.wikiapi.di.ApplicationModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    AndroidSupportInjectionModule::class,
    ActivityBindingModule::class
])

interface ApplicationComponent : AndroidInjector<AppLoader> {
}
