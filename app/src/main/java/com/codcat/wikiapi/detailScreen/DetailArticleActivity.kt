package com.codcat.wikiapi.detailScreen

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.codcat.wikiapi.R
import com.codcat.wikiapi.adapter.IWikiClickListener
import com.codcat.wikiapi.adapter.WikiListAdapter
import com.codcat.wikiapi.data.Article
import com.codcat.wikiapi.utilits.injectViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_detail_article.recyclerImagesList
import kotlinx.android.synthetic.main.activity_detail_article.toolbarImages
import javax.inject.Inject

class DetailArticleActivity : DaggerAppCompatActivity(), IWikiClickListener {


    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject lateinit var linearLayoutManager: LinearLayoutManager
    @Inject lateinit var wikiImagesAdapter: WikiListAdapter

    private lateinit var viewModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_article)

        viewModel = injectViewModel(viewModelFactory)

        initViews()
    }

    private fun initViews() {
        setSupportActionBar(toolbarImages)

        recyclerImagesList.apply {
            adapter = wikiImagesAdapter
            layoutManager = linearLayoutManager
        }
    }

    override fun onWikiClick(article: Article) {

    }

    override fun onWIkiLongClick(article: Article) {

    }
}
