package com.codcat.wikiapi.detailScreen

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import com.codcat.wikiapi.adapter.IWikiClickListener
import com.codcat.wikiapi.adapter.WikiListAdapter
import com.codcat.wikiapi.common.ViewModelFactory
import com.codcat.wikiapi.common.ViewModelKey
import com.codcat.wikiapi.di.scopes.ActivityScope
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap


@Module
abstract class DetailScreenModule {

//    @Binds
//    @ActivityScope
//    abstract fun provideDetailArticleActivity(activity: DetailArticleActivity): DetailArticleActivity

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    internal abstract fun demoViewModel(viewModel: DetailViewModel): ViewModel

    @Binds
    @ActivityScope
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @ActivityScope
    abstract fun bindIWikiClickListener(activity: DetailArticleActivity): IWikiClickListener

    @Module
    companion object {

        @Provides
        @ActivityScope
        @JvmStatic
        fun provideWikiListAdapter(listener: IWikiClickListener): WikiListAdapter = WikiListAdapter(listener)

        @Provides
        @ActivityScope
        @JvmStatic
        fun providerLinearLayoutManager(context: DetailArticleActivity): LinearLayoutManager {
            return LinearLayoutManager(context)
        }
    }
}