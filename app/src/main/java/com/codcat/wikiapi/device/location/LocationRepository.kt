package com.codcat.wikiapi.device.location

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import com.google.android.gms.location.LocationServices
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class LocationRepository @Inject constructor(val context: Context) : ILocationRepository {

    @SuppressLint("MissingPermission")
    override suspend fun getCurrentPosition(): Location = suspendCoroutine { cont ->
        LocationServices.getFusedLocationProviderClient(context).lastLocation
            .addOnSuccessListener { location ->
                cont.resume(location)
            }
//        cont.resumeWithException()
    }
}