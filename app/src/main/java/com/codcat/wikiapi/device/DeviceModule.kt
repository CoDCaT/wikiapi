package com.codcat.wikiapi.device

import android.content.Context
import android.hardware.SensorManager
import android.location.LocationManager
import com.codcat.wikiapi.device.location.ILocationRepository
import com.codcat.wikiapi.device.location.LocationRepository
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.SettingsClient
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class DeviceModule {

    @Binds
    @Singleton
    abstract fun provideLocation(locationRepository: LocationRepository): ILocationRepository

    @Module
    companion object {

        @Provides
        @JvmStatic
        @Singleton
        fun getSystemSensorManager(context: Context): SensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager

        @Provides
        @JvmStatic
        @Singleton
        fun getSystemLocationManager(context: Context): LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        @Provides
        @JvmStatic
        @Singleton
        fun getSettingsClient(context: Context): SettingsClient = LocationServices.getSettingsClient(context)

    }

}