package com.codcat.wikiapi.device.location

import android.location.Location

interface ILocationRepository {

    suspend fun getCurrentPosition(): Location

}