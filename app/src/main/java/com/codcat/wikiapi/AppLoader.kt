package com.codcat.wikiapi

import com.codcat.wikiapi.di.ApplicationModule
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class AppLoader : DaggerApplication() {

    private lateinit var component: ApplicationComponent

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
        return component
    }
}