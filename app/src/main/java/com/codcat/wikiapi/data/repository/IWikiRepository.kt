package com.codcat.wikiapi.data.repository

import com.codcat.wikiapi.data.Article
import kotlinx.coroutines.Deferred

interface IWikiRepository {
    fun getWikiArticleList(lat: Double, lon: Double): Deferred<MutableList<Article>?>
}