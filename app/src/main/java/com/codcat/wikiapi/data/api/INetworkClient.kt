package com.codcat.wikiapi.data.api

import com.codcat.wikiapi.data.pojo.Query
import kotlin.coroutines.CoroutineContext

interface INetworkClient {
    fun getArticles(coroutineContext: CoroutineContext, lat: Double, lon: Double, limit: Int): Query?
}