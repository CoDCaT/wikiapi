package com.codcat.wikiapi.data.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkApi {
    @GET("api.php?action=query&format=json&gsradius=10000")
    fun getArticles(@Query("list") list: String,
                    @Query("ggslimit") limit: String,
                    @Query("gscoord") coord: String): Call<com.codcat.wikiapi.data.pojo.Query>

    //format=json
    //prop=coordinates|pageimages&ggscoord=37.7891838|-122.4033522 [try in ApiSandbox]
    //api.php?action=query&list=geosearch&gscoord=37.7891838|-122.4033522&gsradius=10000&gslimit=10
    //https://en.wikipedia.org/w/api.php?action=query&list=geosearch&gscoord=37.7891838%7C-122.4033522&gsradius=10000&gslimit=1
}