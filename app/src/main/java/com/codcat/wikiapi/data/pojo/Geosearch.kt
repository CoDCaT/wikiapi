package com.codcat.wikiapi.data.pojo

data class Geosearch(
    var pageid: Int = 0,
    var ns: Int = 0,
    var title: String = "",
    var lat: Double = 0.0,
    var lon: Double = 0.0,
    var dist: Int = 0,
    var primary: String = ""
)
