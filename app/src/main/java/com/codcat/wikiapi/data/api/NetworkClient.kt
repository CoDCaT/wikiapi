package com.codcat.wikiapi.data.api

import com.codcat.wikiapi.data.pojo.Query
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.coroutines.CoroutineContext

object NetworkClient : INetworkClient {

    val logging = HttpLoggingInterceptor()
    val httpClient = OkHttpClient.Builder()

    init {
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging)
    }


    private val networkApi = Retrofit.Builder()
        .baseUrl("https://en.wikipedia.org/w/")
        .addConverterFactory(GsonConverterFactory.create())
        .client(httpClient.build())
        .build()
        .create(NetworkApi::class.java)

    override fun getArticles(
        coroutineContext: CoroutineContext,
        lat: Double,
        lon: Double,
        limit: Int
    ): Query? = networkApi
        .getArticles("geosearch", limit.toString(), "$lat|$lon")
        .execute()
        .body()


}