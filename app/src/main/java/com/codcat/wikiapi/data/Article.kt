package com.codcat.wikiapi.data

import android.graphics.Bitmap
import android.net.Uri
import java.util.Date

data class Article @JvmOverloads constructor(
    val id: Int = 0,
    val title: String,
    var thumbnail: Bitmap? = null) {

    override fun equals(other: Any?): Boolean {
        return id == (other as Article).id
    }

    override fun hashCode(): Int {
        return 31 * id.hashCode() + title.hashCode()
    }
}