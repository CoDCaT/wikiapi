package com.codcat.wikiapi.data.pojo

data class Query(
    var geosearch: List<Geosearch> = mutableListOf()
)
