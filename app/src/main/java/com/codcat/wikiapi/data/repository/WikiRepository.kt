package com.codcat.wikiapi.data.repository

import com.codcat.wikiapi.data.Article
import com.codcat.wikiapi.data.api.NetworkClient
import com.codcat.wikiapi.data.pojo.Query
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope.coroutineContext
import kotlinx.coroutines.async
import javax.inject.Inject

class WikiRepository @Inject constructor() : IWikiRepository {

    override fun getWikiArticleList(lat: Double, lon: Double): Deferred<MutableList<Article>?> {

//        return CoroutineScope(coroutineContext).async {
//            val mockList = mutableListOf<Article>()
//            for (i in 0..50L) {
//                mockList.add(Article(id = i, fileName = "name $i"))
//            }
//            mockList
//        }
        return CoroutineScope(coroutineContext).async {
            mapArticle(NetworkClient.getArticles(Dispatchers.IO, lat, lon, LIMIT))
        }
    }

    private fun mapArticle(result: Query?): MutableList<Article>? {
        result?.let { res ->
            return res.geosearch.map { Article(id = it.pageid, title = it.title) }.toMutableList()
        }
        return null
    }

    companion object {
        private const val LIMIT = 50
    }

}