package com.codcat.wikiapi.data

import com.codcat.wikiapi.data.repository.IWikiRepository
import com.codcat.wikiapi.data.repository.WikiRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @Binds
    abstract fun provideWikiRepository(trackListRepository: WikiRepository): IWikiRepository

}