package com.codcat.wikiapi.adapter

import com.codcat.wikiapi.data.Article

interface IWikiClickListener {
    fun onWikiClick(article: Article)
    fun onWIkiLongClick(article: Article)
}