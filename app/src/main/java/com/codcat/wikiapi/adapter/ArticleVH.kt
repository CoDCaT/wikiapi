package com.codcat.wikiapi.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.codcat.wikiapi.R
import com.codcat.wikiapi.data.Article
import kotlinx.android.synthetic.main.article_list_item.view.textArticle
import kotlinx.android.synthetic.main.article_list_item.view.textImageCount

class ArticleVH(val parent: ViewGroup, val clickListener: IWikiClickListener) :
    RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.article_list_item, parent, false)) {

    fun bind(item: Article) {
        itemView.textArticle.text = item.title
        itemView.setOnClickListener { clickListener.onWikiClick(item) }
    }

}