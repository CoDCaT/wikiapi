package com.codcat.wikiapi.adapter

import android.support.v7.recyclerview.extensions.AsyncListDiffer
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.codcat.wikiapi.data.Article


class WikiListAdapter(private val clickListener: IWikiClickListener) : RecyclerView.Adapter<ArticleVH>() {

    private val fileListDiffer = AsyncListDiffer(this, DIFF_CALLBACK)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleVH = ArticleVH(parent, clickListener)

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemCount(): Int = fileListDiffer.currentList.size

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: ArticleVH, position: Int) {
        holder.bind(fileListDiffer.currentList[position])
    }

    fun setFileList(newFileList: MutableList<Article>) {
        fileListDiffer.submitList(newFileList)
    }

    companion object {

        private val DIFF_CALLBACK: DiffUtil.ItemCallback<Article> = object : DiffUtil.ItemCallback<Article>() {
            override fun areItemsTheSame(oldFiles: Article, newFiles: Article): Boolean {
                return oldFiles.id == newFiles.id
            }

            override fun areContentsTheSame(oldFiles: Article, newFiles: Article): Boolean {
                return oldFiles == newFiles
            }
        }
    }
}