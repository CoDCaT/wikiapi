package com.codcat.wikiapi.utilits

import android.content.Context
import android.support.v7.app.AlertDialog
import android.view.View
import com.codcat.wikiapi.R


object DialogManagerUtil {

    fun showPermissionDialog(context: Context, onDenied: () -> Unit, onConfirm: () -> Unit) {
        AlertDialog.Builder(context)
            .setTitle("Permissions")
            .setMessage("Без доступа к GPS, приложение не будет работать, разрешить приложению использовать GPS?")
            .setPositiveButton("Разрешить") { _, _ -> onConfirm.invoke() }
            .setNegativeButton("Выйти") { _, _ -> onDenied.invoke() }
            .setCancelable(false)
            .show()
    }

}