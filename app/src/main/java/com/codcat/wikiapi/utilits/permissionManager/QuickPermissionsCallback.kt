package com.codcat.wikiapi.utilits.permissionManager

interface QuickPermissionsCallback {
    fun shouldShowRequestPermissionsRationale(quickPermissionsRequest: QuickPermissionsRequest?)
    fun onPermissionsGranted(quickPermissionsRequest: QuickPermissionsRequest?)
    fun onPermissionsPermanentlyDenied(quickPermissionsRequest: QuickPermissionsRequest?)
    fun onPermissionsDenied(quickPermissionsRequest: QuickPermissionsRequest?)
}